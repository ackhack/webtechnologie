import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';

import { HomepageComponent } from './pages/homepage/homepage.component';
import { SpeicherService } from './services/SpeicherService.service';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent
],
  imports: [
    BrowserModule,
    FormsModule
],
  providers: [ SpeicherService ],
  bootstrap: [AppComponent]
})

export class AppModule { }
