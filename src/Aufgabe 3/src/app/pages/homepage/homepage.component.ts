import { Component, OnInit } from '@angular/core';
import { SpeicherService } from '../../services/SpeicherService.service';
import { Person } from './../../models/Person';
import { stringify, parse } from 'querystring';
import { SSL_OP_ALL, SIGUSR1 } from 'constants';
import { NgModel } from '@angular/forms';
// RUN WITH 'npx ng serve'

declare var jQuery: any;

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {
    public me: Person;                      // Daten der anzuzeigenden/zu bearbeitenden Person
    public steckBriefGesperrt: boolean;     // Modus: true => nur lesen; false => bearbeiten
    public lastBirthdate: string;           // letzter korrekter Wert für birthdate zum Wiederherstellen
    public fehlermeldung: string;      // Fehlermeldung für Login-Dialog

    // Dialog-Attribute
    public username: string;
    public password: string;

    public constructor(public speicherService: SpeicherService) {
        // Componenten-Daten initialisieren, Personendaten von SpeicherService laden

        /* a) bitte Code hier einfügen... */
        this.me = speicherService.ladePerson();
        this.steckBriefGesperrt = true;
        this.lastBirthdate = this.me.birthdate;
    }

    /*
         * Prueft die eingegebenen Daten (username/password) auf Korrektheit (Hugo/123)
         * und schliesst den Login-Dialog und ruft steckBriefAendern() auf, falls die Daten korrekt sind.
         * Andernfalls wird eine Fehlermeldung angezeigt.
         */
    public login(): void {

        /* b) bitte Code hier einfügen... */

        if (this.username === this.me.username && this.password === this.me.password) {
            this.steckBriefAendern();
            this.cancelLogin();
        } else {
            this.fehlermeldung = 'Falsche Anmeldedaten, bitte erneut versuchen.';
        }

    }

    /*
         * Der Login-Dialog wird geschlossen, die Fehlermeldung wird gelöscht.
         */
    public cancelLogin(): void {
        /*
                 * Login-Dialog verbergen mit jQuery-Aufruf:
                 * es wird das Element mit id 'loginDialog' gesucht und
                 * darauf die Methode 'modal' aufgerufen
                 */
        jQuery('#loginDialog').modal('hide');
        this.fehlermeldung = '';
    }

    /*
         * Prüft zunächst das eingegebene 'birthdate'. Falls ein ungültiger oder leerer Wert vorliegt,
         * wird 'birthdate' aus dem letzten gültigen Wert wiederhergestellt.
         * Abhängig vom aktuellen Modus (Variable 'steckBriefGesperrt' => nur lesen oder bearbeiten)
         * werden nun entweder die geänderten Daten gespeichert und der Bearbeitungsmodus wird
         * verlassen oder es wird vom Lesemodus in den Bearbeitungsmodus gewechselt.
         */
    public steckBriefAendern(): void {

        /* c) bitte Code hier einfügen... */

        console.log(this);
        if (this.steckBriefGesperrt === true) {
            this.steckBriefGesperrt = false;

        } else {

            if (this.me.birthdate === '') {
                this.me.birthdate = this.lastBirthdate;
            } else {
                this.lastBirthdate = this.me.birthdate;
            }

            this.speicherService.speicherePerson(this.me);
            this.steckBriefGesperrt = true;
            this.username = '';
            this.password = '';
        }
    }

    ngOnInit(): void {
    }
}
