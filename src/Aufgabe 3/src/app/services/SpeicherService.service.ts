import { Injectable } from '@angular/core';
import { Person } from '../models/Person';

@Injectable()
export class SpeicherService {
    private imgPath = '/assets/img/';

    public constructor() {
    }

// Lädt die Personendaten aus dem localStorage
    public ladePerson(): Person {
        let person: Person = null;
        /* hier Code ergänzen, nur dummy-Implementierung! */
        if (localStorage.length === 0) {
            person = new Person('Name', 'bart.jpeg', '1970-01-01', 'Place', 'Profession', 'username', 'password');

        } else {
            person = JSON.parse(localStorage.getItem('Person.me'));
        }

        return person;
    }

        // let saved = JSON.parse(localStorage.getItem('Person.me'));
        // person = new Person(saved.name, saved.picturePath, saved.birthdate, saved.bornIn, saved.profession);

// Speichert die übergebenen Personendaten im localStorage
    public speicherePerson(person: Person): void {
        try {
        /* hier Code ergänzen */
        localStorage.setItem('Person.me', JSON.stringify(person));
        } catch (e) {
            console.error('SpeicherService.speicherePerson: error: ' + e);
        }
    }
}
