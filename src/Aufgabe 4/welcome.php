<?php
session_start();

$name = '';
$date = '';
$place = '';
if (isset($_SESSION['name'])) {
    $name = $_SESSION['name'];
}
if (isset($_SESSION['date'])) {
    $date = $_SESSION['date'];
}
if (isset($_SESSION['place'])) {
    $place = $_SESSION['place'];
}
if (isset($_GET['save'])) {
    //TODO add changes
    $_SESSION['name'] = $_GET['name'];
    $_SESSION['date'] = $_GET['date'];
    $_SESSION['place'] = $_GET['place'];
    header("location: index.php");
}
if (isset($_GET['stop'])) {
    header("location: index.php");
}
?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>DataEdit</title>
    <link href="homepage.css" rel="stylesheet">
</head>

<body>
    <div id="peachbox">
        <h1 style="color: grey;text-align: center;">Daten bearbeiten</h1>
        <form action='welcome.php' method='GET'>
            <div>
                <label class='column'><b>Name</b></label>
                <input type='text' name='name' value='<?php echo $name; ?>'>
            </div>
            <div>
                <label class='column'><b>Geburtsdatum</b></label>
                <input type='text' name='date' value='<?php echo $date; ?>'>
            </div>
            <div>
                <label class='column'><b>Geburtsort</b></label>
                <input type='text' name='place' value='<?php echo $place; ?>'>
            </div>
            <div>
                <input type='submit' name='save' value='speichern'>
                <input type='submit' name='stop' value='abbrechen'>
            </div>
        </form>
    </div>
</body>

</html>