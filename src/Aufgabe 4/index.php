<?php
session_start();

$name = '';
$date = '';
$place = '';

if (isset($_SESSION['name'])) {
    $name = $_SESSION['name'];
} else {
    $name = 'Bart Simpson';
    $_SESSION['name'] = $name;
}
if (isset($_SESSION['date'])) {
    $date = $_SESSION['date'];
} else {
    $date = '01.01.1970';
}
if (isset($_SESSION['place'])) {
    $place = $_SESSION['place'];
} else {
    $place = 'Ingolstadt';
}

?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Homepage</title>
    <link href="homepage.css" rel="stylesheet">
</head>

<body>
    <div id="peachbox">
        <h1 style="color: grey;text-align: center;">Willkommen auf der Homepage von <?php echo $name; ?>!</h1>
        <div id="blackbox">
            <div class="tab">
                <a href='index.php'><button class="active">Das bin ich</button></a>
                <a href='past.php'><button class="inactive">Meine Vergangenheit</button></a>
                <a href='love.php'><button class="inactive">Was ich mag</button></a>
            </div>
            <div id="tabc">
                <div id="Steckbrief" style="display: block; padding: 6px 12px;">
                    <h3>Mein Steckbrief:</h3>
                    <div class="column">
                        <img id="picture" src="pic.png" height="100px" width="100px">
                    </div>
                    <div class="column">
                        <form class="form-horizontal" role="form">
                            <div>
                                <label class="column"><b>Name</b></label>
                                <?php echo $name; ?>
                            </div>
                            <div>
                                <label class="column"><b>Geburtsdatum</b></label>
                                <?php echo $date; ?>
                            </div>
                            <div>
                                <label class="column"><b>Ort</b></label>
                                <?php echo $place; ?>
                            </div>
                            <label>. . .</label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <form action="login.php">
            <input type="submit" value="Angaben ändern" style="margin-left: 20px">
        </form>
    </div>
</body>

</html>
