<?php
session_start();

$name = '';

if (isset($_SESSION['name'])) {
    $name = $_SESSION['name'];
}

?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Homepage</title>
    <link href="homepage.css" rel="stylesheet">
</head>

<body>
    <div id="peachbox">
        <h1 style="color: grey;text-align: center;">Willkommen auf der Homepage von <?php echo $name; ?>!</h1>
        <div id="blackbox">
            <div class="tab">
                <a href='index.php'><button class="inactive">Das bin ich</button></a>
                <button class="active">Meine Vergangenheit</button>
                <a href='love.php'><button class="inactive">Was ich mag</button></a>
            </div>
            <div id="tabc">
                <div style="padding: 6px 12px;">
                    <h3>Vergegangenheit</h3>
                    <p>Unileben.txt</p>
                </div>
            </div>
        </div>
        <button type="button" name="changeButton" style="margin-left: 20px;">
            Angaben ändern
        </button>
    </div> <!-- content -->
</body>

</html>