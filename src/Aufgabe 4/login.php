<?php
session_start();

$conn = mysqli_connect('localhost', 'me', 'pw', 'passworddb', '3306');
$sql = 'SELECT password FROM user';

$query = mysqli_query($conn, $sql);
$pw = mysqli_fetch_all($query, MYSQLI_ASSOC);
mysqli_free_result($query);
mysqli_close($conn);


$error = '';
$name = '';

if (isset($_SESSION['name'])) {
    $name = $_SESSION['name'];
}

if (isset($_POST['password'])) {
    if ($_POST['password'] == $pw[0]['password']) {
        header("location: welcome.php");
    } else {
        $error = 'Falsches Passwort!';
    }
}
?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="homepage.css" rel="stylesheet">
</head>

<body>
    <div id="peachbox">
        <h1 style="color: grey;text-align: center;">Login zum Bearbeiten der Daten von <?php echo $name; ?>!</h1>

        <form action="login.php" method="POST">
            <label><b>Passwort: </b></label>
            <input type="password" name="password">
            <input type="submit" name="submit" value="Login">
        </form>
        <p style="color:red"><?php echo $error; ?><p>
    </div>
</body>