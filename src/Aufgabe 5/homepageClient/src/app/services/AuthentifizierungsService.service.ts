import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { SpeicherService } from './SpeicherService.service';

@Injectable()
export class AuthentifizierungsService {
    // Achtung: '/rest' ist wichtig für die Umleitung an den Apache-Server!
    // der Rest des Pfades kann variieren...
    private baseURL = '/rest/homepageServer/';

    public constructor(private http: Http) {
    }

    public authentifiziere(benutzername: string, passwort: string): Promise<boolean> {
        /* hier ihr Code ... */
        let speicher: SpeicherService = new SpeicherService(this.http);
        return speicher.login(benutzername,passwort);
    }
}
