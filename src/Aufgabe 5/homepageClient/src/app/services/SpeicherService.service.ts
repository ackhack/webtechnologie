import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Person } from '../models/Person';

@Injectable()
export class SpeicherService {
    // Achtung: '/rest' ist wichtig für die Umleitung an den Apache-Server!
    // der Rest des Pfades kann variieren...
    private baseURL = '/rest/homepageServer/';

    public constructor(private http: Http) {
    }

    public ladePerson(): Promise<Person> {
        /* hier ihr Code ... */
        return this.http.get(this.baseURL + 'getperson')
            .toPromise()
            .then(response => {
                let b = JSON.parse(response.json());
                return new Person(b.name, b.picturePath, b.birthdate, b.bornIn, b.profession);
            })
            .catch(this.handleError);
    }

    public speicherePerson(person: Person): Promise<Person> {
        /* hier ihr Code ... */
        return this.http.post(this.baseURL + 'putperson', person)
        .toPromise()
        .then((response) => {
            console.log(response);
        }
        )
        .catch(this.handleError);
    }

    public login(benutzername: string, passwort: string): Promise<boolean> {
        /* hier ihr Code ... */
        let user: String[] = [benutzername,passwort];
        return this.http.post(this.baseURL + 'login',user)
            .toPromise()
            .then(response => {
                if (response.status == 200) {
                    return true;
                }
            })
            .catch((err) => {
                return false;
            });
    }

    public handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
