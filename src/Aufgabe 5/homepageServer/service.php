<?php

/**
 * auf true setzen, um DEBUG-Info in das PHP-ErrorLog zu schreiben
 */
$debugToErrorLog = true;
header("Access-Control-Allow-Origin: *");

function __autoload($class_name)
{
    include $class_name . '.php';
}

/*
 * Services
 * 
 */
$pers = Persistenz::getInstance();

/**
 * GET person: liefert eine Personenbeschreibung
 */
function getPerson()
{
    // hier Ihr Code...
    global $pers;
    echo json_encode($pers->ladePerson());
    http_response_code(200);
}

/**
 * PUT person <JSON-Personenbeschreibung>: speichert eine Personenbeschreibung
 */
function putPerson($person)
{
    // hier Ihr Code...
    global $pers;
    $pers->speicherePerson($person);
    http_response_code(200);
}

/**
 * POST login <Benutzername/Passwort>
 * Prüft anhand der in der Datei benutzer.txt abgelegten Daten, ob Benutzername und Passwort korrekt sind.
 * Setzt einen entsprechenden Statuscode, 200: OK, 404: Fehler
 */
function postLogin($data)
{
    // hier Ihr Code...
    global $pers;
    $user = $pers->ladeBenutzer();
    if ($user['benutzername'] != $data[0] || $user['passwort'] != $data[1]) {
        http_response_code(404);
    } else {
        http_response_code(200);
    }
}

/*
 * Service Dispatcher
 */

$url = $_REQUEST['_url'];
$requestType = $_SERVER['REQUEST_METHOD'];
$body = file_get_contents('php://input');
$jsonData = json_decode($body);

if ($GLOBALS["debugToErrorLog"]) {
    error_log("REST-Call: " . $requestType . ' ' . $url . ':' . $body);
}

if ($url != '/') {
    if ($url === '/getperson') {
        // hier ihr Code...
    getPerson();
    } else {
        if ($url === '/putperson') {
            putPerson($jsonData);
        } else {
            if ($url === '/login') {
                postLogin($jsonData);
            } else {
                badRequest($requestType, $url, $body);
            }
        }
    }
}



function badRequest($requestType, $url, $body)
{
    http_response_code(400);
    if ($GLOBALS["debugToErrorLog"]) {
        error_log("bad request");
    }
    die('Ungültiger Carl Request: ' . $requestType . ' , ' . $url . ' , ' . $body);
}
